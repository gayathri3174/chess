# Contributing Guidelines

```markdown
We welcome contributions to this project from everyone! Please take a moment to review this document in order to make the contribution process straightforward and effective for everyone involved.
```
## Contributors

- **Rook Moves**: Gayathri
- **Bishop Moves**: Ganapathy
- **Knight Moves**: Md Farmanul Haque
- **Queen Moves**: Deepika

## How to Contribute

1. Fork the repository to your GitHub account.
2. Clone the forked repository to your local machine.
3. Create a new branch for your changes:
   ```
   git checkout -b feature/<your-branch-name>
   ```
4. Make your changes and commit them to your branch:
   ```
   git commit -am "Add your descriptive commit message"
   ```
5. Push your changes to your forked repository:
   ```
   git push origin feature/<your-branch-name>
   ```
6. Open a pull request (PR) from your forked repository to the main repository.
7. Provide a clear description of your changes in the PR and reference any relevant issues.
8. Your PR will be reviewed by the maintainers, and any necessary feedback or changes will be requested.
9. Once your PR is approved, it will be merged into the main repository.


## Questions

If you have any questions or need further clarification, feel free to reach out to the maintainers or contributors.

Happy contributing!
```

