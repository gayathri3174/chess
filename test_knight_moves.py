import unittest
from knight_moves import knight_moves

class TestKnightMoves(unittest.TestCase):
    
    def test_knight_moves(self):
        # Test case 1: Knight in the middle of the board
        position1 = "d4"
        expected_moves1 = ['f5', 'f3', 'b3', 'b5', 'e6', 'e2', 'c2', 'c6']
        self.assertEqual(knight_moves(position1), expected_moves1)

        # Test case 2: Knight at the edge of the board
        position2 = "h8"
        expected_moves2 = ['f7', 'g6']
        self.assertEqual(knight_moves(position2), expected_moves2)

        # Test case 3: Invalid position
        position3 = "k10"  # Invalid position
        self.assertRaises(ValueError, knight_moves, position3)

if __name__ == '__main__':
    unittest.main()

