import sys

board_size = 8
columns = 'abcdefgh'
rows = '12345678'

def knight_moves(position: str) -> list[str]:
    col_index = columns.index(position[0])
    row_index = rows.index(position[1])

    # Generate all possible moves in L-shape using list comprehensions
    moves = [
        (2, 1), (2, -1), (-2, -1), (-2, 1),  # Horizontal moves with vertical offset
        (1, 2), (1, -2), (-1, -2), (-1, 2)   # Vertical moves with horizontal offset
    ]

    knight_attacking_positions = [
        columns[col_index + col_step] + rows[row_index + row_step]
        for col_step, row_step in moves
        if 0 <= col_index + col_step < board_size and 0 <= row_index + row_step < board_size
    ]

    return knight_attacking_positions

def main():
    if len(sys.argv) != 2:
        print("Usage: python knight_moves.py <position>")
        return
    
    position = sys.argv[1]

    if len(position) != 2 or position[0] not in columns or position[1] not in rows:
        print("Invalid position. Position should be in the format 'a5' to 'h8'.")
        return

    knight_moves_list = knight_moves(position)
    print("Knight moves from position", position + ":", knight_moves_list)

if __name__ == "__main__":
    main()

