# Chess Piece Movement

This repository contains functions to determine possible moves for different chess pieces on an empty chess board. Additionally, there is a function to check if two pieces are attacking each other.

## Functions

### rook_moves(position)
Returns the list of possible moves for a rook from the given position on the chessboard.

### knight_moves(position)
Returns the list of possible moves for a knight from the given position on the chessboard.

### bishop_moves(position)
Returns the list of possible moves for a bishop from the given position on the chessboard.

### queen_moves(position)
Returns the list of possible moves for a queen from the given position on the chessboard.

## How to Use

1. Clone this repository to your local machine:
   ```
   git clone <repository-url>
   ```

2. Import the desired function(s) into your Python script:
   ```python
   from chess_moves import rook_moves, knight_moves, bishop_moves, queen_moves
   ```

3. Use the imported functions to determine the possible moves for different chess pieces at specific positions on the chessboard.

## Example
```python
position = "d4"

rook_possible_moves = rook_moves(position)
print("Rook Possible Moves:", rook_possible_moves)

knight_possible_moves = knight_moves(position)
print("Knight Possible Moves:", knight_possible_moves)

bishop_possible_moves = bishop_moves(position)
print("Bishop Possible Moves:", bishop_possible_moves)

queen_possible_moves = queen_moves(position)
print("Queen Possible Moves:", queen_possible_moves)
```

## Contributing

Contributions are welcome! If you'd like to contribute to this project, please refer to the [CONTRIBUTOR.md](contributor.md) file for guidelines.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
